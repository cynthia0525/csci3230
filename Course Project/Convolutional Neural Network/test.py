import tensorflow as tf

from cnn import Tox21CNN
from constants import *
from data_processing import load_csv, write_txt

# train / test / score
MODE = 'score'

# CSV
dataset = load_csv(MODE)

# Tensors
input_tensor = tf.placeholder(tf.float32, shape=dataset['tensor']['input'])
probability = tf.placeholder(tf.float32)

# CNN
cnn = Tox21CNN(HIDDEN_LAYER, WEIGHTS, BIASES)
prediction = cnn.convolutional_neural_network(input_tensor, probability)

sess = tf.InteractiveSession()

# Initialization
sess.run(tf.global_variables_initializer())

# Modelling
ckpt = tf.train.get_checkpoint_state('.')
saver = tf.train.import_meta_graph(ckpt.model_checkpoint_path + '.meta')
saver.restore(sess, ckpt.model_checkpoint_path)

# Testing
prediction = prediction.eval(feed_dict={input_tensor: dataset['input'], probability: 1.0})

# Analysis
write_txt(prediction)

sess.close()
