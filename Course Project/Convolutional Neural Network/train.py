import math
import tensorflow as tf

from cnn import Tox21CNN
from constants import *
from data_processing import load_csv, write_txt

# CSV -- train
print('\n-- Start to read training data')
dataset = load_csv('train')

# Tensors
input_tensor = tf.placeholder(tf.float32, shape=dataset['tensor']['input'])
output_tensor = tf.placeholder(tf.float32, shape=dataset['tensor']['output'])
probability = tf.placeholder(tf.float32)

# CNN
cnn = Tox21CNN(HIDDEN_LAYER, WEIGHTS, BIASES)
prediction, optimization, accuracy = cnn.network_measurement(input_tensor, output_tensor, probability, LEARNING_RATE)

sess = tf.InteractiveSession()

# Initialization
sess.run(tf.global_variables_initializer())

print('\n-- Start to train the model')
for batch in range(math.ceil(dataset['size'] / BATCH_SIZE)):
    # Range of the current batch
    lower_bound = batch * BATCH_SIZE
    upper_bound = min((batch + 1) * BATCH_SIZE, dataset['size'])
    # I/O for the current batch
    input_batch = dataset['input'][lower_bound: upper_bound]
    output_batch = dataset['output'][lower_bound: upper_bound]
    # Training
    optimization.run(feed_dict={input_tensor: input_batch, output_tensor: output_batch, probability: 0.5})
    training_accuracy = accuracy.eval(feed_dict={input_tensor: input_batch, output_tensor: output_batch,
                                                 probability: 1.0})
    # Analysis
    print("[Iteration {:2}] Accuracy={:.5f}".format(batch, training_accuracy))

# CSV -- test
print('\n-- Start to read testing data')
dataset = load_csv('test')

print('\n-- Start to test the model')
result, testing_accuracy = sess.run([prediction, accuracy], feed_dict={input_tensor: dataset['input'],
                                                                       output_tensor: dataset['output'],
                                                                       probability: 1.0})
write_txt(result)
print("Accuracy={:.5f}".format(testing_accuracy))

# Modelling
saver = tf.train.Saver()
saver.save(sess, './tox21', global_step=1)

sess.close()
