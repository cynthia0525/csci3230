import numpy as np

from constants import *


def load_csv(mode):
    dataset = {'tensor': {}}
    for file_type in TOX21_AR:
        if mode is 'score' and file_type is 'output':
            continue
        raw_data = {}
        for key in TOX21_AR[file_type]:
            config = TOX21_AR[file_type][key]
            data = read_csv(mode, key, config['header'])
            data = format_csv(file_type, data, config['shape'])
            print(mode, file_type, key, data.shape)
            raw_data[key] = data
        dataset[file_type] = process_data(file_type, raw_data)
        dataset['tensor'][file_type] = np.append([None], dataset[file_type].shape[1:])
        print('>>> ', dataset[file_type].shape)
    dataset['size'] = dataset['input'].shape[0]
    return dataset


def read_csv(mode, file, header):
    filename = '../Tox21_AR/{}_{}.csv'.format(mode, file)
    if header:
        return np.genfromtxt(open(filename, 'rb'), dtype=int, delimiter=',', skip_header=1, usecols=(1,))
    else:
        return np.genfromtxt(open(filename, 'rb'), dtype=int, delimiter=',', skip_header=0)


def format_csv(mode, data, shape):
    if mode is 'input':
        return np.reshape(data, shape)
    else:
        return np.eye(CLASS_SIZE)[data]


def write_txt(data):
    print('[prediction]', data.shape)
    labels = np.argmax(data, 1)
    np.savetxt('labels.txt', labels, delimiter='\n', fmt='%1d')


def process_data(mode, data):
    if mode is 'input':
        duplicate = np.tile(data['nodes'], NODE_SIZE)
        row = duplicate.reshape((-1, NODE_SIZE, NODE_SIZE, 1))
        col = row.transpose((0, 2, 1, 3))
        nodes = np.add(row, col)
        return np.add(data['graphs'], nodes)
    else:
        return data['labels']
