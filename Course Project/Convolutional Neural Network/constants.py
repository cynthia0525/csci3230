# Input & Output Constants
NODE_SIZE = 132
CLASS_SIZE = 2
TOX21_AR = {'output': {'labels': {'header': True, 'shape': [-1, CLASS_SIZE]}},
            'input': {'graphs': {'header': False, 'shape': [-1, NODE_SIZE, NODE_SIZE, 1]},
                      'nodes': {'header': True, 'shape': [-1, NODE_SIZE, 1]}}}

# Network Constants
LEARNING_RATE = 0.001
BATCH_SIZE = 100

# Layer Constants
HIDDEN_LAYER = 2
WEIGHTS = [[4, 4, 1, 4], [4, 4, 4, 12], [33 * 33 * 12, 132], [132, 2]]
# WEIGHTS = [[4, 4, 1, 4], [66 * 66 * 4, 132], [132, 2]]
BIASES = [[4], [12], [132], [2]]
# BIASES = [[4], [132], [2]]
