import tensorflow as tf


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def convolution(input_layer, weight, bias):
    convolutional_layer = tf.nn.conv2d(input_layer, weight, strides=[1, 1, 1, 1], padding='SAME')
    return tf.nn.relu(convolutional_layer + bias)


def pooling(input_layer):
    return tf.nn.max_pool(input_layer, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')


def connection(input_layer, weight, bias):
    return tf.matmul(input_layer, weight) + bias


class Tox21CNN:
    def __init__(self, hidden_layer, weights, biases):
        self.hidden_layer = hidden_layer
        self.dense_shape = [-1, weights[-2][0]]
        self.weights = [weight_variable(w) for w in weights]
        self.biases = [bias_variable(b) for b in biases]

    def convolutional_neural_network(self, input_tensor, probability):
        # Input Layer
        input_layer = input_tensor

        # Convolutional & Pooling Layers
        for index in range(self.hidden_layer):
            convolutional_layer = convolution(input_layer, self.weights[index], self.biases[index])
            pooling_layer = pooling(convolutional_layer)
            input_layer = pooling_layer

        # Dense Layer
        dense_layer = tf.reshape(input_layer, self.dense_shape)
        dense_layer = connection(dense_layer, self.weights[-2], self.biases[-2])
        dense_layer = tf.nn.relu(dense_layer)

        # Dropout Layer
        dropout_layer = tf.nn.dropout(dense_layer, probability)

        # Output Layer
        return connection(dropout_layer, self.weights[-1], self.biases[-1])

    def network_measurement(self, input_tensor, output_tensor, probability, learning_rate):
        # Prediction
        prediction = self.convolutional_neural_network(input_tensor, probability)
        loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=prediction, labels=output_tensor))

        # Optimization
        optimization = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

        # Evaluation
        evaluation = tf.equal(tf.argmax(prediction, 1), tf.argmax(output_tensor, 1))
        accuracy = tf.reduce_mean(tf.cast(evaluation, tf.float32))

        # Analysis
        return prediction, optimization, accuracy
