# CSCI3230 Fundamentals of Artificial Intelligence (Fall 2019)
Basic concepts and techniques of artificial intelligence. Knowledge representation: predicate logic and inference, semantic networks, scripts and frames, and object-oriented representation. Searching: such as A*, hill-climbing, minimax and alpha-beta pruning. Planning: the frame problem and the STRIPS formalism, representation schemes and planning strategies. Neural networks: learning algorithms, neural architecture and applications. Natural language processing. Knowledge acquisition and expert systems: properties, techniques and tools of expert systems.

# Grading
* 30% Written Assignments (3×10%)
* 15% Prolog Assignment [90/100]
* 30% Course Project [17.95/20]
* 25% Term Paper