% Direction States
direction(anti-clockwise).
direction(clockwise).
direction_group(Direction,OppositeDirection) :-
	direction(Direction), direction(OppositeDirection), not(Direction==OppositeDirection).


% Quadrant States
quadrant(top-left).
quadrant(top-right).
quadrant(bottom-left).
quadrant(bottom-right).

quadrant_group(1,13,top-left).
quadrant_group(2,7,top-left).
quadrant_group(3,1,top-left).
quadrant_group(7,14,top-left).
quadrant_group(9,2,top-left).
quadrant_group(13,15,top-left).
quadrant_group(14,9,top-left).
quadrant_group(15,3,top-left).

quadrant_group(4,16,top-right).
quadrant_group(5,10,top-right).
quadrant_group(6,4,top-right).
quadrant_group(10,17,top-right).
quadrant_group(12,5,top-right).
quadrant_group(16,18,top-right).
quadrant_group(17,12,top-right).
quadrant_group(18,6,top-right).

quadrant_group(19,31,bottom-left).
quadrant_group(20,25,bottom-left).
quadrant_group(21,19,bottom-left).
quadrant_group(25,32,bottom-left).
quadrant_group(27,20,bottom-left).
quadrant_group(31,33,bottom-left).
quadrant_group(32,27,bottom-left).
quadrant_group(33,21,bottom-left).

quadrant_group(22,34,bottom-right).
quadrant_group(23,28,bottom-right).
quadrant_group(24,22,bottom-right).
quadrant_group(28,35,bottom-right).
quadrant_group(30,23,bottom-right).
quadrant_group(34,36,bottom-right).
quadrant_group(35,30,bottom-right).
quadrant_group(36,24,bottom-right).

quadrant_group(Grid,Grid,Quadrant) :- quadrant(Quadrant).


% Winning States
win([1,7,13,19,25]).
win([5,11,17,23,29]).
win([8,9,10,11,12]).
win([13,14,15,16,17]).
win([1,2,3,4,5]).
win([5,10,15,20,25]).
win([8,15,22,29,36]).
win([14,15,16,17,18]).
win([1,8,15,22,29]).
win([6,12,18,24,30]).
win([9,15,21,27,33]).
win([19,20,21,22,23]).
win([2,8,14,20,26]).
win([6,11,16,21,26]).
win([10,16,22,28,34]).
win([20,21,22,23,24]).
win([2,3,4,5,6]).
win([7,13,19,25,31]).
win([11,17,23,29,35]).
win([25,26,27,28,29]).
win([2,9,16,23,30]).
win([7,8,9,10,11]).
win([11,16,21,26,31]).
win([26,27,28,29,30]).
win([3,9,15,21,27]).
win([7,14,21,28,35]).
win([12,18,24,30,36]).
win([31,32,33,34,35]).
win([4,10,16,22,28]).
win([8,14,20,26,32]).
win([12,17,22,27,32]).
win([32,33,34,35,36]).


% Rotation
rotate(List,RotatedList,anti-clockwise,Quadrant) :-
	rotate_once(List,NewList,Quadrant),
	sort(NewList,RotatedList).
rotate(List,RotatedList,clockwise,Quadrant) :-
	rotate_once(List,TempList1,Quadrant),
	rotate_once(TempList1,TempList2,Quadrant),
	rotate_once(TempList2,NewList,Quadrant),
	sort(NewList,RotatedList).

rotate_once([],[],Quadrant) :- quadrant(Quadrant).
rotate_once([H|T],RotatedList,Quadrant) :-
	quadrant_group(H,R,Quadrant),
	rotate_once(T,TempList,Quadrant),
	append([R],TempList,RotatedList),
	!.


% Next Threat
next_threat(_,[],[]).
next_threat(Player,[H|T],ThreatL) :-
	threatening(H,Player,Threat),
	next_threat(Player,T,TempList),
	append([Threat],TempList,ThreatL),
	!.


% Helper Functions
threatening_row(Player,Opponent,ThreateningMove) :-
	win(WinningState),
	intersection(WinningState,Player,ThreateningState),
	length(ThreateningState,4),
	subtract(WinningState,ThreateningState,[ThreateningMove|_]),
	not(member(ThreateningMove,Opponent)).

place_to_win(Player,Opponent,Position,NextPlayer) :-
	threatening_row(Player,Opponent,Position),
	append(Player,[Position],NewPlayer),
	sort(NewPlayer,NextPlayer).

rotate_to_win(Player,Opponent,move(Position,Direction,Quadrant),NextPlayer,NextOpponent) :-
	direction_group(Direction,OppositeDirection),
	quadrant(Quadrant),
	rotate(Player,NewPlayer,Direction,Quadrant),
	rotate(Opponent,NextOpponent,Direction,Quadrant),
	place_to_win(NewPlayer,NextOpponent,_,NextPlayer),
	rotate(NextPlayer,RotatedPlayer,OppositeDirection,Quadrant),
	sort(RotatedPlayer,PlacedPlayer),
	subtract(PlacedPlayer,Player,[Position|_]).

random_move(Player,Opponent,move(Position,Direction,Quadrant),NextPlayer,NextOpponent) :-
	between(1,36,Position),
	not(member(Position,Player)),
	not(member(Position,Opponent)),
	append(Player,[Position],NewPlayer),
	direction(Direction),
	quadrant(Quadrant),
	rotate(NewPlayer,RotatedPlayer,Direction,Quadrant),
	rotate(Opponent,RotatedOpponent,Direction,Quadrant),
	sort(RotatedPlayer,NextPlayer),
	sort(RotatedOpponent,NextOpponent).

look_ahead(Player,Opponent,move(Position,Direction,Quadrant),NextPlayer,NextOpponent) :-
	random_move(Player,Opponent,move(Position,Direction,Quadrant),NextPlayer,NextOpponent),
	findall(OppoPlaceM,place_to_win(NextOpponent,NextPlayer,OppoPlaceM,_),PlaceList),
	length(PlaceList,0),
	findall(OppoRotateM,rotate_to_win(NextOpponent,NextPlayer,move(OppoRotateM,_,_),_,_),RotateList),
	length(RotateList,0).

minimax(Player,BoardL,NextBoard,Minimax) :-
	next_threat(Player,BoardL,ThreatL),
	Minimax -> max_list(ThreatL,ThreatNum); min_list(ThreatL,ThreatNum),
	threatening(NextBoard,Player,ThreatNum).


% Threatening -- threatening(Board,CurrentPlayer,ThreatsCount).
threatening(board(BlackL,RedL),black,ThreatsCount) :-
	findall(ThreateningMove,threatening_row(RedL,BlackL,ThreateningMove),ThreatsList),
	length(ThreatsList,ThreatsCount).
threatening(board(BlackL,RedL),red,ThreatsCount) :-
	findall(ThreateningMove,threatening_row(BlackL,RedL,ThreateningMove),ThreatsList),
	length(ThreatsList,ThreatsCount).


% Best Move -- pentago_ai(Board,CurrentPlayer,BestMove,NextBoard).
pentago_ai(board(BlackL,RedL),black,move(Position,Direction,Quadrant),board(NextBlackL,RedL)) :-
	direction(Direction),
	quadrant(Quadrant),
	place_to_win(BlackL,RedL,Position,NextBlackL), !.
pentago_ai(board(BlackL,RedL),red,move(Position,Direction,Quadrant),board(BlackL,NextRedL)) :-
	direction(Direction),
	quadrant(Quadrant),
	place_to_win(RedL,BlackL,Position,NextRedL), !.
pentago_ai(board(BlackL,RedL),black,move(Position,Direction,Quadrant),board(NextBlackL,NextRedL)) :-
	rotate_to_win(BlackL,RedL,move(Position,Direction,Quadrant),NextBlackL,NextRedL), !.
pentago_ai(board(BlackL,RedL),red,move(Position,Direction,Quadrant),board(NextBlackL,NextRedL)) :-
	rotate_to_win(RedL,BlackL,move(Position,Direction,Quadrant),NextRedL,NextBlackL), !.

pentago_ai(board(BlackL,RedL),black,move(Position,Direction,Quadrant),board(NextBlackL,NextRedL)) :-
	findall(board(NextBlack,NextRed),
		look_ahead(BlackL,RedL,move(_,_,_),NextBlack,NextRed),
		NextBoardL),
	minimax(red,NextBoardL,board(NextBlackL,NextRedL),true),
	look_ahead(BlackL,RedL,move(Position,Direction,Quadrant),NextBlackL,NextRedL),
	findall(board(NextNextBlack,NextNextRed),
		look_ahead(NextRedL,NextBlackL,move(_,_,_),NextNextRed,NextNextBlack),
		NextNextBoardL),
	minimax(black,NextNextBoardL,board(NextNextBlackL,NextNextRedL),false),
	look_ahead(NextRedL,NextBlackL,move(_,_,_),NextNextRedL,NextNextBlackL), !.
pentago_ai(board(BlackL,RedL),red,move(Position,Direction,Quadrant),board(NextBlackL,NextRedL)) :-
	findall(board(NextBlack,NextRed),
		look_ahead(RedL,BlackL,move(_,_,_),NextRed,NextBlack),
		NextBoardL),
	minimax(black,NextBoardL,board(NextBlackL,NextRedL),true),
	look_ahead(RedL,BlackL,move(Position,Direction,Quadrant),NextRedL,NextBlackL),
	findall(board(NextNextBlack,NextNextRed),
		look_ahead(NextBlackL,NextRedL,move(_,_,_),NextNextBlack,NextNextRed),
		NextNextBoardL),
	minimax(red,NextNextBoardL,board(NextNextBlackL,NextNextRedL),false),
	look_ahead(NextBlackL,NextRedL,move(_,_,_),NextNextBlackL,NextNextRedL), !.

pentago_ai(board(BlackL,RedL),black,move(Position,Direction,Quadrant),board(NextBlackL,NextRedL)) :-
	random_move(BlackL,RedL,move(Position,Direction,Quadrant),NextBlackL,NextRedL), !.
pentago_ai(board(BlackL,RedL),red,move(Position,Direction,Quadrant),board(NextBlackL,NextRedL)) :-
	random_move(RedL,BlackL,move(Position,Direction,Quadrant),NextRedL,NextBlackL), !.

















